package com.github.test.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.github.test.entity.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hxq40
 * @since 2018-11-16
 */
public interface UserMapper extends BaseMapper<User> {

}
