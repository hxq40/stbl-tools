package com.github.test.controller;
import java.util.Map;
import java.util.Date;

import com.github.test.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.github.test.entity.User;
import stbl.common.util.Query;
import stbl.common.util.R;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hxq40
 * @since 2018-11-16
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired private IUserService userService;

    /**
    * 通过ID查询
    *
    * @param id ID
    * @return User
    */
    @GetMapping("/{id}")
    public R<User> get(@PathVariable Integer id) {
        return new R<>(userService.selectById(id));
    }


    /**
    * 分页查询信息
    *
    * @param params 分页对象
    * @return 分页对象
    */
    @RequestMapping("/page")
    public Page page(@RequestParam Map<String, Object> params) {
        //params.put(CommonConstant.DEL_FLAG, CommonConstant.STATUS_NORMAL);
        return userService.selectPage(new Query<>(params), new EntityWrapper<>());
    }

    /**
     * 添加
     * @param  user  实体
     * @return success/false
     */
    @PostMapping
    public R<Boolean> add(@RequestBody User user) {
        return new R<>(userService.insert(user));
    }

    /**
     * 删除
     * @param id ID
     * @return success/false
     */
    @DeleteMapping("/{id}")
    public R<Boolean> delete(@PathVariable Integer id) {
        User user = new User();
        user.setId((long)id);
        //user.setUpdateTime(new Date());
        //user.setDelFlag(CommonConstant.STATUS_DEL);
        return new R<>(userService.updateById(user));
    }

    /**
     * 编辑
     * @param  user  实体
     * @return success/false
     */
    @PutMapping
    public R<Boolean> edit(@RequestBody User user) {
        //user.setUpdateTime(new Date());
        return new R<>(userService.updateById(user));
    }
}
