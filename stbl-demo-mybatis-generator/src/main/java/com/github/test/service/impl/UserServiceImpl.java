package com.github.test.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.test.entity.User;
import com.github.test.mapper.UserMapper;
import com.github.test.service.IUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hxq40
 * @since 2018-11-16
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
