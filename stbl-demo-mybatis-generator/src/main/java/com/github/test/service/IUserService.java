package com.github.test.service;

import com.baomidou.mybatisplus.service.IService;
import com.github.test.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hxq40
 * @since 2018-11-16
 */
public interface IUserService extends IService<User> {

}
