package ${package.Controller};
import java.util.Map;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import stbl.common.util.R;
import ${package.Entity}.${entity};
import ${package.Service}.I${entity}Service;
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * <p>
 * ${table.comment} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@RestController
@RequestMapping("/${table.entityPath}")
public class ${table.controllerName} extends ${superControllerClass} {
    @Autowired private I${entity}Service ${table.entityPath}Service;

    /**
    * 通过ID查询
    *
    * @param id ID
    * @return ${entity}
    */
    @GetMapping("/{id}")
    public R<${entity}> get(@PathVariable Long id) {
        return new R<>(${table.entityPath}Service.getById(id));
    }


    /**
    * 分页查询信息
    *
    * @param params 分页对象
    * @return 分页对象
    */
    @RequestMapping("/page")
    public IPage page(@RequestParam Map<String, Object> params) {
        //params.put(CommonConstant.DEL_FLAG, CommonConstant.STATUS_NORMAL);
        return ${table.entityPath}Service.page(
                                            new Page<${entity}>(1,10),
                                            new QueryWrapper<${entity}>());
    }

    /**
     * 添加
     * @param  ${table.entityPath}  实体
     * @return success/false
     */
    @PostMapping
    public R<Boolean> add(@RequestBody ${entity} ${table.entityPath}) {
        return new R<>(${table.entityPath}Service.save(${table.entityPath}));
    }

    /**
     * 删除
     * @param id ID
     * @return success/false
     */
    @DeleteMapping("/{id}")
    public R<Boolean> delete(@PathVariable Long id) {
        ${entity} ${table.entityPath} = new ${entity}();
        ${table.entityPath}.setId(id);
        ${table.entityPath}.setUpdateTime(LocalDateTime.now());
        //${table.entityPath}.setDelFlag(CommonConstant.STATUS_DEL);
        return new R<>(${table.entityPath}Service.updateById(${table.entityPath}));
    }

    /**
     * 编辑
     * @param  ${table.entityPath}  实体
     * @return success/false
     */
    @PutMapping
    public R<Boolean> edit(@RequestBody ${entity} ${table.entityPath}) {
        ${table.entityPath}.setUpdateTime(LocalDateTime.now());
        return new R<>(${table.entityPath}Service.updateById(${table.entityPath}));
    }
}
